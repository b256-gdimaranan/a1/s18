// console.log("Hellow");

// Section funcuion paraemetes and arguments -------
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
			// Functions are mostly created to create complicated tasks to run several lines of code in succession
			// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

// we also learedn in the previoys session that we can gather data fom user input using the promt windows

function printInput() {
	let nickName = prompt("Enter your nickname:");
	console.log("Hi, " + nickName);
}
printInput();

//However, for some use cases, this may not be ideal. 
			//For other cases, functions can also process data directly passed into it instead of relying only on Global Variables and prompt().

//name is called parameter
// A "parameter" acts as a named variable/container that exists only inside of a function
	// It is used to store information that is provided to a function when it is called/invoked.
function printName(name) {
	console.log("My name is " + name);
}
//"Juana", the information/data provided directly into the function is called an argument.
	//Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.
printName("Juana"); //the dinction can pass the daya intp the function printName
printName("John");
printName("Jane");

// variable can slo be passed as an argument
let sampleVariable = "Meredith";

printName(sampleVariable);
//Function arguments cannot be used by a function if there are no parameters provided within the function.

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + "divided by 8 is " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is" + num + " divisibilty by 8?")
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);
checkDivisibilityBy8(103);

// Section function as Arguments --------
// Function parameters can also accept other functions as arguments
			// Some complex functions use other functions as arguments to perform more complicated results
			// This will be further seen when we discuss array methods.

function argumentFunction() {
	console.log("This function is passed as an argument before the message was printed")

}
function invokeFunction(argumentFunction) {
	// when we are using parenthis (), it is invoking a function
	argumentFunction();
}
// when we are ot using the parenthesis (), it associated as using it the function as an argument to anothe function
invokeFunction(argumentFunction);

console.log(argumentFunction);

// Using multiple paremeters ------------------
// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order

function createFullName(firstName, middlename, lastName) {
	console.log(firstName +" "+ middlename +" "+lastName)
// "Juan" will be stored in the parameter "firstName"
			// "Dela" will be stored in the parameter "middleName"
			// "Cruz" will be stored in the parameter "lastName"
}
createFullName('juan','cruz', 'dela')

createFullName("Juan","dela")
// variable as argument
let fName = "John";
let mName = "Doe";
let lName = "Smith";

createFullName(fName,mName,lName);

// the return statements --------------
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

function returnFullName(firstName,middlename,lastName) {
	return firstName + " " + middlename + " " + lastName;
	console.log("I am here but not seen");

}

let completeName = returnFullName("meredith", "grey", "shepherd")
console.log(completeName);

console.log(returnFullName("meredith", "grey", "shepherd"))
//You can also create a variable inside the function to contain the result and return that variable instead.
function returnAddress(city,country) {
	let fullAddress = city + " " + country;
	return fullAddress;
}
let myAddress = returnAddress("Manile", "philippines")
console.log(myAddress);
//On the other hand, when a function only has the console.log() to display its result it will return undefined instead.
function printPlayerInfo(uname,level,job) {
	console.log(uname);
	console.log(level);
	console.log(job);
}
// returns undefined because printPlayerInfo returns nothing. It only console.logs the details.
	// You cannot save any value from printPlayerInfo() because it does not return anything.
let user1 = printPlayerInfo("white_knight",80,"paladin");
console.log(user1);

